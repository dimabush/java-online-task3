package hw.exceptions;

public class MyAutoCloseable implements AutoCloseable {
  @Override
  public void close() throws Exception {
    throw new MyException();
  }
}
