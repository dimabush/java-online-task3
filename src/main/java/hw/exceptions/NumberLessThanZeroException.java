package hw.exceptions;

public class NumberLessThanZeroException extends Exception {
  public NumberLessThanZeroException() {
    super("Current number is less than zero");
  }

  public NumberLessThanZeroException(String message) {
    super(message);
  }

  public NumberLessThanZeroException(String message, Throwable cause) {
    super(message, cause);
  }

  public NumberLessThanZeroException(Throwable cause) {
    super(cause);
  }
}
