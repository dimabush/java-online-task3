package hw.exceptions;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {

    int num;
    try (MyAutoCloseable myAutoCloseable = new MyAutoCloseable();
         BufferedWriter writer = new BufferedWriter(new FileWriter("test.txt", true))
    ) {
      try (Scanner scanner = new Scanner(System.in)) {
        num = scanner.nextInt();
      } catch (InputMismatchException e) {
        throw new InputWrongValueException();
      }
      if (num < 0) {
        throw new NumberLessThanZeroException();
      } else {
        writer.write("Number = " + num + ", ");
      }
    } catch (FileNotFoundException e) {
      File file = new File("test.txt");
    } catch (InputWrongValueException | NumberLessThanZeroException | MyException | IOException e) {
      e.printStackTrace();
      System.out.println(e.getCause());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
