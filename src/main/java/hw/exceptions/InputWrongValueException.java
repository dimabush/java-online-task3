package hw.exceptions;

public class InputWrongValueException extends Exception {
  public InputWrongValueException() {
    super("Entered value should be integer type");
  }

  public InputWrongValueException(String message) {
    super(message);
  }

  public InputWrongValueException(String message, Throwable cause) {
    super(message, cause);
  }

  public InputWrongValueException(Throwable cause) {
    super(cause);
  }
}
